#!/bin/bash

# Prepare setup and run tests for the WordPress plugin "Another simple image optimizer"
#
# Usage:
# The script uses relative paths in many places. It must be run from `/var/www/html`.
# Expected usage is calling it from the outside with the default work dir set to `/var/www/html`:
# `docker exec wpimgopt_php81 wp-content/plugins/another-simple-image-optimizer/tests/prepare-and-run-tests.sh`
#
# Notes:
# - $WP_PHP_VERSION is set via docker-compose.yml
# - $WP_VERSIONS is set via docker-compose.yml (comma separated string)
#
# TODO: add `-h` and/or `help` cli arg

# exit script if a command fails
set -e

# print all commands (for debugging)
# set -x

# make sure, that the scripts runs in the right folder (usage of relative paths...)
cd /var/www/html

# convert string to array - $WP_VERSIONS is set in docker-compose.yml, which
# doesn't support arrays in env vars)
IFS=\, read -a WP_VERSIONS <<<"$WP_VERSIONS"

PLUGIN_NAME=another-simple-image-optimizer
DOCS_ROOT=/var/www/html/
PLUGIN_DIR=wp-content/plugins/

# Some colors in console output
# Usage: `echo -e " $ICON_FAIL Message"`
COLOR_FAIL='\033[0;31m'     # Red
COLOR_SUCCESS='\033[0;32m'  # Green
COLOR_RESET='\033[0m'       # Text Reset
ICON_FAIL="${COLOR_FAIL}x${COLOR_RESET}"
ICON_SUCCESS="${COLOR_SUCCESS}✔${COLOR_RESET}"

# Incompatible container has `ASIO_CAN_PROC_OPEN` set to false to fake a
# disabled `proc_open`. This container runs different tests
IS_INCOMPATIBLE=false
[[ "$WP_PHP_VERSION" == "8.3_incompatible" ]] && IS_INCOMPATIBLE=true
PORT_INCOMPATIBLE="${PORT_INCOMPATIBLE:-8089}"

enable_automatic_login() {
  mkdir -p wp-content/mu-plugins
  cat > wp-content/mu-plugins/skip-login.php << 'PHP'
<?php

/**
 * Skip login page and login as admin automatically
 *
 * works on `/`, but not on `/wp-admin/` because
 * of `auth_redirect()` in `wp-includes/pluggable.php`
 */
add_action('init', function() {
  if (is_user_logged_in()) return;
  $user = get_user_by('login', 'admin' );
  wp_set_current_user($user->ID);
  wp_set_auth_cookie($user->ID, true);
});

/**
 * Overwrite/disable `auth_redirect()` to prevent it from redirecting to `wp-login.php`
 */
function auth_redirect() {}
PHP
}

enable_svg_avif_upload() {
  mkdir -p wp-content/mu-plugins
  cat > wp-content/mu-plugins/enable-svg-upload.php << 'PHP'
<?php

/**
 * Allow uploading SVG and AVIF files
 */
add_filter('upload_mimes', function($mimeTypes) {
  $mimeTypes['svg']  = 'image/svg+xml';

  $hasNativeAvifSupport = version_compare($GLOBALS['wp_version'], '6.5', '>=');
  if (!$hasNativeAvifSupport) {
    $mimeTypes['avif'] = 'image/avif';
  }
  return $mimeTypes;
});

/**
 * Fix missing 'file' key for SVG
 * Hook fires before img opt plugin
 */
add_filter('wp_generate_attachment_metadata', function($meta, $id, $context) {

  if (isset($meta['file'])) return $meta;

  $post = get_post($id);

  $isAttachment = $post->post_type === 'attachment';
  $isSVG  = $isAttachment && $post->post_mime_type === 'image/svg+xml';
  $isAvif = $isAttachment && $post->post_mime_type === 'image/avif';

  if (!($isSVG || $isAvif)) return $meta;

  $hasNativeAvifSupport = version_compare($GLOBALS['wp_version'], '6.5', '>=');
  if ($isAvif && $hasNativeAvifSupport) {
    return $meta;
  }

  $uploadUrl = wp_upload_dir()['baseurl'];
  $file = trim(str_replace($uploadUrl, '', $post->guid), '/');
  $meta['file'] = $file;

  // width/height of avif works if PHP>=8.2

  if ($isSVG) {
    // Quick and dirty hard coded dimensions
    // This is no real svg support plugin, so it doesn't make sense to parse
    // the XML structure. Also in a real world application, the SVG should be
    // sanitized before saving. In this case parsing the width and height would
    // make more sense during the sanitization step.
    $meta['width']  = 100;
    $meta['height'] = 100;
  }

  return $meta;

}, 9, 3);

/**
 * Try to generate image meta data for avif files (if PHP >=8.2)
 */
add_filter('file_is_displayable_image', function($result, $path) {

  $hasNativeAvifSupport = version_compare($GLOBALS['wp_version'], '6.5', '>=');
  if ($hasNativeAvifSupport) return $result;

  // getimagesize supports avif correctly since PHP 8.2
  if (PHP_VERSION_ID < 80200) return $result;

  $info = @getimagesize($path);

  if (empty($info)) return $result;

  $isAvif = $info[2] === IMAGETYPE_AVIF;

  if (!$isAvif) return $result;

  return true;

}, 10, 2);
PHP
}

prepare_wp_installation() {

  VERSION_DIR=wp_$VERSION
  cd $VERSION_DIR

  # don't prepare again when calling test script multiple times
  if wp db check > /dev/null 2>&1 ; then
    cd ..
    return;
  fi

  cp ../wp-config.php wp-config.php

  wp config set DB_NAME "php_${WP_PHP_VERSION}_${VERSION_DIR}" --quiet

  if [ "$VERSION" = "6.0" ] ; then
    # disable deprecated notices or Requests library - should be fixed in WP 6.2 and wp-cli 2.8.0
    # Will produce false positive results, because debug.log is disabled
    # TODO: reimplement error_reporting and debug.log
    # TODO: maybe skip the whole WP_DEBUG_LOG nonsense and use the default php error log instead
    # @see https://core.trac.wordpress.org/ticket/54504
    # @see https://github.com/wp-cli/wp-cli/issues/5613
    wp config set WP_DEBUG false --raw --quiet
  fi

  wp db create --quiet

  PORT=8080
  # TODO: extract port automatically from version string
  if [ "$WP_PHP_VERSION" = "8.1" ] ; then PORT=8081
  elif [ "$WP_PHP_VERSION" = "8.2" ] ; then PORT=8082
  elif [ "$WP_PHP_VERSION" = "8.3" ] ; then PORT=8083
  fi
  # Use custom port for incompatible container
  $IS_INCOMPATIBLE && PORT="$PORT_INCOMPATIBLE"

  URL="localhost:${PORT}/${VERSION_DIR}"

  wp core install --url=${URL} --title="wp img opt test" --admin_user=admin --admin_password=admin --admin_email=test@example.org --quiet

  # symlink plugin (mounted as docker volume) into version sub folders
  ln -s ${DOCS_ROOT}${PLUGIN_DIR}${PLUGIN_NAME} ${DOCS_ROOT}${VERSION_DIR}/${PLUGIN_DIR}${PLUGIN_NAME}

  enable_automatic_login
  enable_svg_avif_upload

#   wp plugin activate $PLUGIN_NAME --quiet

  echo "Prepared host (WP: ${VERSION} ($(wp core version)), PHP: ${WP_PHP_VERSION}), http://${URL}/"

  cd ..
}

test_optimize_existing_images() {
  VERSION_DIR=wp_$VERSION
  cd $VERSION_DIR

  echo "Test: Import images, than activate plugin, than regenerate media (WP: ${VERSION} ($(wp core version)), PHP: ${WP_PHP_VERSION}):"

  declare -A IMG_SIZES
  declare -A IMG_PATHS

  wp plugin deactivate $PLUGIN_NAME --quiet

  # import images and store paths/file sizes for later comparison
  for FILE in ${PLUGIN_DIR}${PLUGIN_NAME}/tests/images/* ; do
    if [ -f $FILE ] ; then

      FILE_NAME=$(basename $FILE)
      FILE_EXT="${FILE_NAME##*.}"

      # TODO: check for error message, e. g. if file type is not allowed
      IMG_ID=$(wp media import --porcelain "${FILE}")

      IMG_PATH=$(wp post meta get $IMG_ID _wp_attached_file)
      IMG_PATH_REL=${VERSION_DIR}/wp-content/uploads/${IMG_PATH}
      IMG_PATH_ABS=${DOCS_ROOT}${IMG_PATH_REL}

      IMG_SIZES["$FILE"]=$(stat -c%s "$IMG_PATH_ABS")
      IMG_PATHS["$FILE"]=$IMG_PATH_ABS
    fi
  done

  # regenerate imported images with activated plugin and compare sizes
  wp plugin activate $PLUGIN_NAME --quiet
  wp media regenerate --yes --quiet
  for FILE in ${PLUGIN_DIR}${PLUGIN_NAME}/tests/images/* ; do
    if [ -f $FILE ] ; then

      FILE_NAME=$(basename $FILE)
      FILE_EXT="${FILE_NAME##*.}"

      FILE_SIZE=${IMG_SIZES["$FILE"]}
      IMG_PATH_ABS=${IMG_PATHS["$FILE"]}
      NEW_FILE_SIZE=$(stat -c%s "$IMG_PATH_ABS")

      if [ $NEW_FILE_SIZE -lt $FILE_SIZE ] ; then
        echo -e " ${ICON_SUCCESS} [${FILE_EXT}] File size is smaller (${FILE_SIZE}/${NEW_FILE_SIZE})"
      else
        echo -e " ${ICON_FAIL} [${FILE_EXT}] File size is not smaller (${FILE_SIZE}/${NEW_FILE_SIZE})"
      fi
    fi
  done

  cd ..
}

test_optimize_new_images() {
  VERSION_DIR=wp_$VERSION
  cd $VERSION_DIR

  echo "Test: Import images while plugin is activated (WP: ${VERSION} ($(wp core version)), PHP: ${WP_PHP_VERSION}):"

  wp plugin activate $PLUGIN_NAME --quiet

  # import images and store paths/file sizes for later comparison
  for FILE in ${PLUGIN_DIR}${PLUGIN_NAME}/tests/images/* ; do
    if [ -f $FILE ] ; then

      FILE_NAME=$(basename $FILE)

      FILE_EXT="${FILE_NAME##*.}"

      # TODO: check for error message, e. g. if file type is not allowed
      IMG_ID=$(wp media import --porcelain "${FILE}")

      IMG_PATH=$(wp post meta get $IMG_ID _wp_attached_file)
      IMG_PATH_REL=${VERSION_DIR}/wp-content/uploads/${IMG_PATH}
      IMG_PATH_ABS=${DOCS_ROOT}${IMG_PATH_REL}

      FILE_SIZE=$(stat -c%s "$FILE")
      NEW_FILE_SIZE=$(stat -c%s "$IMG_PATH_ABS")

      if [ $NEW_FILE_SIZE -lt $FILE_SIZE ] ; then
        echo -e " ${ICON_SUCCESS} [${FILE_EXT}] File size is smaller (${FILE_SIZE}/${NEW_FILE_SIZE})"
      else
        echo -e " ${ICON_FAIL} [${FILE_EXT}] File size is not smaller (${FILE_SIZE}/${NEW_FILE_SIZE})"
      fi
    fi
  done

  cd ..
}

test_compare_resize_quality() {
  VERSION_DIR=wp_$VERSION
  cd $VERSION_DIR

  echo "Test: Compare resize quality of jpeg file (WP: ${VERSION} ($(wp core version)), PHP: ${WP_PHP_VERSION}):"

  wp plugin deactivate $PLUGIN_NAME --quiet

  FILE="${PLUGIN_DIR}${PLUGIN_NAME}/tests/images/31x37px_9.svg.php_painted.jpg"
  IMG_ID=$(wp media import --porcelain "${FILE}")
  IMG_META=$(wp post meta get "$IMG_ID" _wp_attachment_metadata --format=json)

  wp plugin activate $PLUGIN_NAME --quiet

  declare -A FILE_SIZES
  declare -A FILE_SIZES_DISABLED
  QUALITIES=(82 90 100)
  SIZES=(thumbnail medium medium_large)

  for SIZE in "${SIZES[@]}" ; do
    FILE_SIZES_DISABLED["${SIZE}"]=$(echo "$IMG_META" | jq -r ".sizes.${SIZE}.filesize")
  done

  TABLE_FORMAT="%-12s %-12s %-12s %s\n"
  printf "$TABLE_FORMAT" "Quality/Size" "${SIZES[@]}"
  printf "$TABLE_FORMAT" "disabled" "${FILE_SIZES_DISABLED["thumbnail"]}" "${FILE_SIZES_DISABLED["medium"]}" "${FILE_SIZES_DISABLED["medium_large"]}"

  for QUALITY in "${QUALITIES[@]}" ; do

    $(export ASIO_RESIZE_QUALITY_JPEG="$QUALITY"; wp media regenerate "$IMG_ID" --quiet)

    IMG_META=$(wp post meta get $IMG_ID _wp_attachment_metadata --format=json)

    for SIZE in "${SIZES[@]}" ; do
      FILE_SIZES["$SIZE"]=$(echo "$IMG_META" | jq -r ".sizes.${SIZE}.filesize")
    done

    printf "$TABLE_FORMAT" "${QUALITY}" "${FILE_SIZES["thumbnail"]}" "${FILE_SIZES["medium"]}" "${FILE_SIZES["medium_large"]}"
  done

  cd ..
}

test_plugin_urls() {

  VERSION_DIR=wp_$VERSION
  cd $VERSION_DIR

  wp plugin activate $PLUGIN_NAME --quiet

  echo "Test: Call affected plugin urls (WP: ${VERSION} ($(wp core version)), PHP: ${WP_PHP_VERSION}):"

  # Call affected urls (media library, settings page) with curl
  # Errors during page rendering will result in a non-empty debug.log,
  # which is printed at the end of the test
  TEST_URLS=(
    '/wp-admin/upload.php?mode=list'
    '/wp-admin/options-general.php?page=simple-image-optimizer'
    '/wp-admin/options-general.php?page=simple-image-optimizer&action=optimize'
  )

  # inside docker container, the port is 80 instead of e. g. 8081
  BASE_URL="http://localhost/${VERSION_DIR}"
  for TEST_URL in "${TEST_URLS[@]}"
  do
    STATUS_CODE=$(curl --write-out '%{http_code}' --silent --output /dev/null ${BASE_URL}/${TEST_URL})
    if [ $STATUS_CODE -eq 200 ] ; then
      echo -e " ${ICON_SUCCESS} Called: ${TEST_URL}"
    else
      echo -e " ${ICON_FAIL} Called: ${TEST_URL}"
    fi
  done

  cd ..
}

test_optimize_link_in_media_library() {
  VERSION_DIR=wp_$VERSION
  cd $VERSION_DIR

  echo "Test: Import media (plugin deactivated), then call 'optimize' link in media library (WP: ${VERSION} ($(wp core version)), PHP: ${WP_PHP_VERSION}):"

  # jpg as base test, svg and avif because they fail with `wp media regenerate`,
  # which has a different implementation than core WP
  for FILE in ${PLUGIN_DIR}${PLUGIN_NAME}/tests/images/*{.jpg,.svg,.avif} ; do
    wp plugin deactivate $PLUGIN_NAME --quiet

    FILE_NAME=$(basename $FILE)
    FILE_EXT="${FILE_NAME##*.}"

    # TODO: check for error message, e. g. if file type is not allowed
    IMG_ID=$(wp media import --porcelain "${FILE}")

    wp plugin activate $PLUGIN_NAME --quiet

    # inside docker container, the port is 80 instead of e. g. 8081
    BASE_URL="http://localhost/${VERSION_DIR}"

    # get media library list view
    MEDIA_URL='/wp-admin/upload.php?mode=list'
    MEDIA_HTML=$(curl "${BASE_URL}/${MEDIA_URL}" --silent)

    if ! $IS_INCOMPATIBLE ; then

      # get nonce token from media library list view
      NONCE_TOKEN=$(echo "${MEDIA_HTML}" | grep -oP "action=optimize&#038;id=${IMG_ID}&#038;_wpnonce=\K.*(?=\")")
      TEST_URL="/wp-admin/options-general.php?page=simple-image-optimizer&action=optimize&id=${IMG_ID}&_wpnonce=${NONCE_TOKEN}"
      STATUS_CODE=$(curl --write-out '%{http_code}' --silent --output /dev/null ${BASE_URL}/${TEST_URL})

      # TODO: check status code, eventually grep for specific output

      IMG_PATH=$(wp post meta get $IMG_ID _wp_attached_file)
      IMG_PATH_REL=${VERSION_DIR}/wp-content/uploads/${IMG_PATH}
      IMG_PATH_ABS=${DOCS_ROOT}${IMG_PATH_REL}

      FILE_SIZE=$(stat -c%s "$FILE")
      NEW_FILE_SIZE=$(stat -c%s "$IMG_PATH_ABS")

      if [ $NEW_FILE_SIZE -lt $FILE_SIZE ] ; then
        echo -e " ${ICON_SUCCESS} [${FILE_EXT}] File size is smaller (${FILE_SIZE}/${NEW_FILE_SIZE})"
      else
        echo -e " ${ICON_FAIL} [${FILE_EXT}] File size is not smaller (${FILE_SIZE}/${NEW_FILE_SIZE})"
      fi

    else
      { echo "${MEDIA_HTML}" | grep -oPq '<td[^>]*>.*<a [^>]*href="[^"]*action=optimize[^>]*>Optimize<\/a>'; } \
        && echo -e " ${ICON_FAIL} 'Optimize' link found while in incompatibility mode" \
        || echo -e " ${ICON_SUCCESS} No 'Optimize' link found while in incompatibility mode"
    fi

  done

  cd ..

}

test_debug_log() {

  VERSION_DIR=wp_$VERSION
  cd $VERSION_DIR

  echo "Test: debug.log (WP: ${VERSION} ($(wp core version)), PHP: ${WP_PHP_VERSION}):"

  # print debug.log
  if [ -s wp-content/debug.log ] ; then
    echo -e " ${ICON_FAIL} Errors in debug.log:"
    cat wp-content/debug.log
  else
    echo -e " ${ICON_SUCCESS} No errors in debug.log"
  fi

  cd ..
}

# wait (and suppress error messages) while db container starts spinning up
# check is against the default table "wordpress" (via docker-compose.yml)
# with the default `wp-config.php` from WP docker image
while ((i++)); ! wp db check > /dev/null 2>&1 ; do
# while ! wp db check ; do
  [ $i -eq 1 ] && echo "Waiting for database connection..."
  sleep 2
done


# Delete debug.log files with `./test.sh delete-logs`
if [ $# -gt 0 ] && [ $1 = 'delete-logs' ] ; then
  for VERSION in "${WP_VERSIONS[@]}"
  do
    FILE_NAME="wp_${VERSION}/wp-content/debug.log"
    [ -f $FILE_NAME ] && rm $FILE_NAME && echo "Deleted debug.log - WP ${VERSION} (PHP ${WP_PHP_VERSION})"
  done
fi


for VERSION in "${WP_VERSIONS[@]}"
do
  prepare_wp_installation
done
if [ $# -gt 0 ] && [ $1 = 'prepare' ] ; then
  exit
fi

# TODO: Add cli arg to run only one or some tests
for VERSION in "${WP_VERSIONS[@]}"
do
  if $IS_INCOMPATIBLE ; then
    test_plugin_urls
    echo
    test_optimize_link_in_media_library
    echo
    test_debug_log
    echo
  else
    test_optimize_existing_images
    echo
    test_optimize_new_images
    echo
    test_compare_resize_quality
    echo
    test_plugin_urls
    echo
    test_optimize_link_in_media_library
    echo
    test_debug_log
    echo
  fi
done
