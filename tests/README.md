# Tests for Another simple image optimizer

## Usage

Docker and docker compose are required.

Tested on OpenSUSE Tumbleweed. Not sure, if this works with Windows or Mac.

### Run tests

```bash
cd tests

# (re)build docker images with latest WP releases
docker compose build

./test.sh

# delete debug.log files
./test.sh delete-logs

# Only prepare hosts, don't run tests
./test.sh prepare
```

Now all instances are also available via browser for manual tests:

* PHP 8.1
  * `http://localhost:8081/wp_6.0/`
  * `http://localhost:8081/wp_latest/`
  * `http://localhost:8081/wp_nightly/`
  * `http://localhost:8081/wp_beta/`
* PHP 8.2
  * `http://localhost:8082/wp_6.0/`
  * `http://localhost:8082/wp_latest/`
  * `http://localhost:8082/wp_nightly/`
  * `http://localhost:8082/wp_beta/`
* PHP 8.3
  * `http://localhost:8083/wp_latest/`
  * `http://localhost:8083/wp_nightly/`
  * `http://localhost:8083/wp_beta/`
* Incompatible (PHP 8.3, only latest WP, disabled `proc_open` is faked via `define('ASIO_CAN_PROC_OPEN', false)`)
  * `http://localhost:8089/wp_latest/`

### Remove containers and database volume

```bash
docker compose down -v
```

## Test matrix

 PHP/WP | 6.0 | latest | nightly | beta
--------|-----|--------|---------|-----
8.1     |  x  |   x    |   x     |  x
8.2     |  x  |   x    |   x     |  x
8.3     |     |   x    |   x     |  x
9.0     |     |        |         |

* one docker container per PHP version
* each container has all WP versions

## Tests/Workflows

* [x] Import images, than activate plugin, than regenerate media
* [x] Import images while plugin is activated
* [x] Call affected plugin urls
* [x] Import media (plugin deactivated), then call 'optimize' link in media library
* [x] Check, if debug.log is not empty
* [ ] Import optimized image, test if replacement is skipped

## Development

```bash
docker compose build
# or
docker compose build --no-cache

# not tested, yet --> not possible with docker compose
# docker build --target latest

# not tested, yet --> not possible with docker compose
# docker build --no-cache-filter=latest,nightly
```

### Useful snippets

Enter container:

```bash
docker exec wpimgopt_php81 bash
# docker exec wpimgopt_php82 bash
# docker exec wpimgopt_php83 bash
```

Enter dev folder inside container:

```bash
# cd wp_6.0
cd wp_latest
# cd wp_nightly
# cd wp_beta
```

enable bash completion for wp cli inside container:

```bash
source /usr/share/bash-completion/completions/wp-cli
```

Other:

```bash
wp plugin activate another-simple-image-optimizer
wp plugin deactivate another-simple-image-optimizer
wp media import wp-content/plugins/another-simple-image-optimizer/tests/images/the-night-song-of-the-fish.png
wp media regenerate --yes
```

```bash
wp media import wp-content/plugins/another-simple-image-optimizer/tests/images/31x37px_9.svg.php_painted.jpg
wp post meta get 4 _wp_attachment_metadata --format=json | jq '.'
```

## Test images

The screenshot of "The night song of the fish" is a transformation of the German visual poem "Fisches Nachtgesang" by Christian Morgenstern into hex. The correct translation should be "Fish's Night Song". This was inspired by a friends' blog post [Casting poems into hexadecimal](https://pycache.de/hex_lyrics/).

The variants of "31x37px_9.svg" are from a work at the urban art festival "ibug" in 2015 at "Alte Kaffeerösterei" (en: "old coffee roasters") in Plauen, Germany. That project felt appropriate as test images for an image compression plugin. If your interested and if you speak German, I wrote [a blog post about that code wall](https://www.raffael.one/blog/2015/12/05/ibug2015_code-wand).

License for all test images: [CC-BY](https://creativecommons.org/licenses/by/4.0/) Raffael Jesche.

## TODO

* [ ] add method to run single test or only some tests
* [x] install missing image optimizer libs
  * [x] jpegoptim
  * [x] optipng
  * [x] pngquant
  * [x] gifsicle
  * [x] cwebp
  * [x] svgo
  * [x] avifenc
  * [x] avifdec
* [x] add missing image formats
  * [x] jpg
  * [x] png
  * [x] gif
  * [x] webp
  * [x] avif
  * [x] svg

### Notes, links

Not used, because I use `wp core download` in Dockerfile, but interesting for reference:

* latest nightly:
  * https://wordpress.org/nightly-builds/wordpress-latest.zip
* json to latest stable ("http" without "s")
  * http://api.wordpress.org/core/version-check/1.7/
* json with multiple versions ("https" url)
  * https://api.wordpress.org/core/version-check/1.7/
* json with latest beta/rc release
  * https://api.wordpress.org/core/version-check/1.7/?channel=beta
