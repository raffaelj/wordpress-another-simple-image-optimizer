#!/bin/bash

docker compose up -d

docker exec wpimgopt_php81 wp-content/plugins/another-simple-image-optimizer/tests/prepare-and-run-tests.sh "$@"

docker exec wpimgopt_php82 wp-content/plugins/another-simple-image-optimizer/tests/prepare-and-run-tests.sh "$@"

docker exec wpimgopt_php83 wp-content/plugins/another-simple-image-optimizer/tests/prepare-and-run-tests.sh "$@"

docker exec wpimgopt_php83_incompatible wp-content/plugins/another-simple-image-optimizer/tests/prepare-and-run-tests.sh "$@"
