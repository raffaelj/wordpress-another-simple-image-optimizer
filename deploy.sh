#!/bin/bash

# TODO: check, if version matches plugin.php and readme.txt

TMP_SVN_DIR=wp-svn

SVN_REPO_URL=https://plugins.svn.wordpress.org/another-simple-image-optimizer

SVN_USER=raffaelj

CURRENT_DIR=$PWD

# create or update svn dir
if [ -d "$TMP_SVN_DIR/.svn" ]; then
    cd $TMP_SVN_DIR
    svn update
    cd $CURRENT_DIR
else
    svn checkout $SVN_REPO_URL $TMP_SVN_DIR
fi

# get latest tag (also works, if on different branch)
WP_PLUGIN_VERSION=`git describe --tags $(git rev-list --tags --max-count=1)`

echo "Current version: $WP_PLUGIN_VERSION"

if [ -d "$TMP_SVN_DIR/tags/$WP_PLUGIN_VERSION" ]; then
    echo "$WP_PLUGIN_VERSION already exists locally."
else
    # clear trunk folder
    rm -r $TMP_SVN_DIR/trunk && mkdir $TMP_SVN_DIR/trunk

    # copy current version to trunk
    git archive $WP_PLUGIN_VERSION | tar -x -C $TMP_SVN_DIR/trunk


    cd $TMP_SVN_DIR

    # add all local changes and suppress error message "... is already under version control"
    svn add trunk/* --force

    echo "Copying trunk to tags/$WP_PLUGIN_VERSION"
    svn copy trunk tags/$WP_PLUGIN_VERSION
fi

if [ "$PWD" != "$CURRENT_DIR/$TMP_SVN_DIR" ]; then
    cd $TMP_SVN_DIR
fi


STATUS=`svn info tags/$WP_PLUGIN_VERSION | grep "^Schedule: " | awk '{print $2}'`

if [ "$STATUS" = "normal" ]; then
    echo "$WP_PLUGIN_VERSION already exists on remote."
    echo "Nothing to to."
    exit
elif [ "$STATUS" = "add" ]; then
    read -r -p "Commit to SVN? (y/n) " SHOULD_COMMIT

    if [ "$SHOULD_COMMIT" = "y" ]; then
        if svn commit -m "bump version to $WP_PLUGIN_VERSION" --username $SVN_USER
        then echo "Updated on remote to $WP_PLUGIN_VERSION"
        else echo "Updating aborted."
        fi
    else
        echo "Commit aborted."
    fi
fi
